package com.server.service;

import com.server.model.Vehicle;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class VehicleService {
    /*
    @Autowired
    private VehicleDao vehicleDao;
    */

    private static VehicleService vehicleService = null;
    private final ArrayList<Vehicle> vehicles = new ArrayList<>();

    public static VehicleService getInstance()
    {
        if (vehicleService == null)
            vehicleService = new VehicleService();
        return vehicleService;
    }

    // getByID
    public Vehicle getVehicle(Long identifier) {
        if (identifier != null)
            return vehicles.get(Math.toIntExact(identifier));
        return null;
    }

    // getByName
    public Vehicle getVehicle(String name) {
        if (name != null) {
            for (Vehicle vehicle:vehicles) {
                if (vehicle.getName().equals(name))
                    return vehicle;
            }
        }
        return null;
    }

    // getAll
    public ArrayList<Vehicle> getVehicle() {
            return vehicles;
    }

    // insert
    public int insertVehicle(Vehicle vehicle) {
        vehicles.add(vehicle);
        return vehicles.size() - 1;
    }


}
