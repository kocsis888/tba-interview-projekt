package com.server.service;

import com.server.model.Waypoint;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class WaypointService {
    /*
    @Autowired
    private WaypointDao waypointDao;
    */
    private static WaypointService waypointService = null;
    private static final ArrayList<Waypoint> waypoints = new ArrayList<>();

    public static WaypointService getInstance()
    {
        if (waypointService == null)
            waypointService = new WaypointService();
        return waypointService;
    }

    // getByID
    public Waypoint getWaypoint(Long identifier) {
        if (identifier != null)
            return waypoints.get(Math.toIntExact(identifier));
        return null;
    }

    // getByName
    public Waypoint getWaypoint(String name) {
        if (name != null) {
            for (Waypoint waypoint:waypoints) {
                if (waypoint.getName().equals(name))
                    return waypoint;
            }
        }
        return null;
    }

    // getAll
    public ArrayList<Waypoint> getWaypoint() {
        return waypoints;
    }

    // insert
    public void insertWaypoint(Waypoint waypoint) {
        waypoints.add(waypoint);
        System.out.println("WAYPOINT INSERTED!");
    }

}
