package com.server.model;

public class Waypoint extends BaseClass {
    private double x;
    private double y;
    private String name;

    public Waypoint(double x, double y, String name) {
        this.x = x;
        this.y = y;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Waypoint(Long identifier, double x, double y) {
        super();
        this.setIdentifier(identifier);
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

}
