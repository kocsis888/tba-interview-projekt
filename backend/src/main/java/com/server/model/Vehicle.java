package com.server.model;


import javafx.scene.paint.Color;

public class Vehicle extends BaseClass {
    // Name
    private String name;
    // Speed
    private double speed;
    // Color in UI
    private Color color;
    // Vehicle current position in the field
    private Waypoint currentPos;
    // Index of the destination waypoint
    private Waypoint destination;

    public Vehicle(String name, double speed, Color color,
                   Waypoint currentPos, Waypoint destination) {
        super();
        this.name = name;
        this.speed = speed;
        this.color = color;
        this.destination = destination;
        this.currentPos = currentPos;
    }

    public Waypoint getDestination() {
        return destination;
    }

    public void setDestination(Waypoint destination) {
        this.destination = destination;
    }

    public Waypoint getCurrentPos() {
        return currentPos;
    }

    public void setCurrentPos(Waypoint currentPos) {
        this.currentPos = currentPos;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

}
