package com.server.model;

import javafx.scene.layout.StackPane;
import javafx.scene.shape.Line;

public class RenderedVehicle {
    private String vehicleName;
    private StackPane stackPane;
    private Line line;

    public RenderedVehicle(String vehicle, StackPane stackPane, Line line) {
        this.vehicleName = vehicle;
        this.stackPane = stackPane;
        this.line = line;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicle) {
        this.vehicleName = vehicle;
    }

    public StackPane getStackPane() {
        return stackPane;
    }

    public void setStackPane(StackPane stackPane) {
        this.stackPane = stackPane;
    }

    public Line getLine() {
        return line;
    }

    public void setLine(Line line) {
        this.line = line;
    }
}
