package com.server.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InjectionPoint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import java.lang.reflect.Field;
/*
@Configuration
public class LoggerBuilder {
    @Bean
    @Scope("prototype")
    public Logger buildLogger(InjectionPoint injectionPoint) {
        Field field = injectionPoint.getField();
        if (field != null) {
            Class<?> temp = field.getDeclaringClass();
            if (temp != null) {
                return LoggerFactory.getLogger(temp);
            }
        }
        return null;
    }
}
*/