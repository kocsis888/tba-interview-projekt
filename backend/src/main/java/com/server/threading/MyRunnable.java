package com.server.threading;

import com.server.model.Vehicle;
import com.server.model.Waypoint;

import java.util.concurrent.locks.ReadWriteLock;


// update Vehicles current position, calculate next position that it needs to take,
public class MyRunnable implements Runnable {
    Vehicle vehicle;
    ReadWriteLock  lock;

    public MyRunnable(Vehicle vehicle, ReadWriteLock lock) {
        this.vehicle = vehicle;
        this.lock = lock;
    }

    @Override
    public void run() {
        lock.writeLock().lock();
        lock.readLock().lock();

        // if the vehicle is idle
        if (vehicle.getDestination() == null) {
            lock.writeLock().unlock();
            lock.readLock().unlock();
            return;
        }

        // arrived at the destination
        if (Math.abs(vehicle.getCurrentPos().getX() - vehicle.getDestination().getX()) < 1  &&
                Math.abs(vehicle.getCurrentPos().getY() - vehicle.getDestination().getY()) < 1) {
            vehicle.setCurrentPos(vehicle.getDestination());
            vehicle.setDestination(null);
            System.out.println("DONE:" + vehicle.getName());
            lock.writeLock().unlock();
            lock.readLock().unlock();
            return;
        }

        // get next point between the destination and the current position
        Waypoint pointBetween = Equations.getNextPointBetween(
                vehicle.getCurrentPos(),
                vehicle.getDestination(),
                vehicle.getSpeed()
        );

        vehicle.setCurrentPos(pointBetween);
        lock.writeLock().unlock();
        lock.readLock().unlock();
    }
}
