package com.server.threading;

import com.server.model.Waypoint;

public class Equations {

    // Get points between two waypoints
    public static Waypoint getNextPointBetween(Waypoint waypoint1, Waypoint waypoint2, double speed) {
        double slope = (waypoint1.getY() - waypoint2.getY()) / (waypoint1.getX() - waypoint2.getX());
        // max speed is 200, max value it should impact is 0.9
        double speedFactor = (speed * 0.9) / 200;
        // Slope intercept formula
        double tempX = waypoint1.getX();
        double tempY = waypoint1.getY();

        if (tempX == waypoint2.getX()) {
            if (tempY < waypoint2.getY())
                tempY += speedFactor;
            else
                tempY -= speedFactor;
        }
        else if (tempY == waypoint2.getY()) {
            if (tempX < waypoint2.getX())
                tempX += speedFactor;
            else
                tempX -= speedFactor;
        }
        else {
            if (tempX < waypoint2.getX())
                tempX += speedFactor;
            else
                tempX -= speedFactor;
            tempY = slope * (tempX - waypoint1.getX()) + waypoint1.getY();
        }

        return new Waypoint(tempX, tempY, "");
    }

    // get distance between 2 points
    public static double getDistanceBetween(Waypoint waypoint1, Waypoint waypoint2) {
        double x1 = waypoint1.getX();
        double x2 = waypoint2.getX();
        double y1 = waypoint1.getY();
        double y2 = waypoint1.getY();
        return Math.sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1));
    }

    // get angle between 2 points
    public static double getRotationAngle(Waypoint waypoint1, Waypoint waypoint2) {
        double angle = (waypoint1.getY() - waypoint2.getY()) / (waypoint1.getX() - waypoint2.getX());
        return Math.pow(Math.tan(angle), - 1);
    }
}
