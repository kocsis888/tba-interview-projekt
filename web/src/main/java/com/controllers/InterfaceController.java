package com.controllers;

import com.server.model.RenderedVehicle;
import com.server.model.Vehicle;
import com.server.model.Waypoint;
import com.server.service.VehicleService;
import com.server.service.WaypointService;
import com.server.threading.MyRunnable;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class InterfaceController {
    @FXML
    private ColorPicker colorPickerBox;
    @FXML
    private Button insertVehicleButton;
    @FXML
    private TextField speedBox;
    @FXML
    private TextField xFieldWaypoint;
    @FXML
    private TextField yFieldWaypoint;
    @FXML
    private Button insertWaypointButton;
    @FXML
    private ListView<String> waypointListView;
    @FXML
    private TextField waypointNameBox;
    @FXML
    private TextField vehicleNameBox;
    @FXML
    private ScrollPane scrollPane;
    @FXML
    private AnchorPane renderPane;
    @FXML
    private ComboBox<String> generalVehicleComboBox;
    @FXML
    private TextField generalVehicleSpeed;
    @FXML
    private TextField generalVehicleName;
    @FXML
    private TextField generalCurrentPosition;
    @FXML
    private TextField generalDestination;
    @FXML
    private ListView<String> generalWaypointListview;
    @FXML
    private Button generalChangeDestinationButton;


    private final VehicleService vehicleService = VehicleService.getInstance();
    private final WaypointService waypointService = WaypointService.getInstance();
    private final ArrayList<RenderedVehicle> renderedVehicles = new ArrayList<>();
    private boolean speedInFocus = false;
    ReadWriteLock lock = new ReentrantReadWriteLock();

    public InterfaceController() {}

    @FXML
    public void initialize() {
        // init text fields
        xFieldWaypoint.setText("0");
        yFieldWaypoint.setText("0");
        speedBox.setText("0");
        generalVehicleSpeed.setText("");

        // add a few waypoint and render them
        waypointService.insertWaypoint(new Waypoint(100, 100, "TEST1"));
        waypointService.insertWaypoint(new Waypoint(100, 700, "TEST2"));
        waypointService.insertWaypoint(new Waypoint(700, 100, "TEST3"));
        waypointService.insertWaypoint(new Waypoint(300, 300, "TEST4"));

        ArrayList<Waypoint> waypoints = waypointService.getWaypoint();
        for (Waypoint waypoint:waypoints) {
            waypointListView.getItems().add(waypoint.getName());
            generalWaypointListview.getItems().add(waypoint.getName());
            renderWaypoint(waypoint);
        }

        // periodically update render data and data about the selected vehicle;
        // threads do the work, then when they are done, update the render pane and the UI with the new data
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                startThreads();
            }
        }, 0, 100);
        renderPane.setBackground(new Background(
                new BackgroundFill(Color.GREY, null, null)));
    }

    // start up threads that calculate the next step of every vehicle, then after they are finished update UI and render
    public void startThreads() {
        ExecutorService executorService = Executors.newCachedThreadPool();
        Vehicle vehicle;
        try {
            if (renderedVehicles.size() == 0)
                return;
            // start up a thread for every rendered Vehicle
            for (RenderedVehicle renderedVehicle : renderedVehicles) {
                vehicle = vehicleService.getVehicle(renderedVehicle.getVehicleName());
                executorService.execute(new MyRunnable(vehicle, lock));
            }
            executorService.shutdown();
            boolean finished = executorService.awaitTermination(1, TimeUnit.MINUTES);
            // theoretically all threads have finished and wont interfere with the update methods
            Platform.runLater(() -> {
                updateRenderScreen();
                generalLoadVehicle(null);
            });
        } catch (InterruptedException e) {
            System.out.println("START THREADS: " + e);
        }
    }

    // insert a vehicle from the UI based on the completed fields
    @FXML
    public void insertVehicle(MouseEvent event) {
        try {
            // min speed is 0, max speed
            if (Double.parseDouble(speedBox.getText()) <= 0 || Double.parseDouble(speedBox.getText()) > 200)
                return;
            // Forbid inserting 2 vehicles with the same name
            if (vehicleService.getVehicle(vehicleNameBox.getText()) != null)
                return;
            // if no starting waypoint is selected
            if (waypointListView.getSelectionModel().getSelectedItem() == null)
                return;
            Vehicle vehicle = new Vehicle(
                    vehicleNameBox.getText(),
                    Double.parseDouble(speedBox.getText()),
                    colorPickerBox.getValue(),
                    waypointService.getWaypoint(waypointListView.getSelectionModel().getSelectedItem()),
                    null
            );
            vehicleService.insertVehicle(vehicle);
            generalVehicleComboBox.getItems().add(vehicleNameBox.getText());
            vehicleNameBox.setText("");
            startVehicle(vehicle);
        } catch (NumberFormatException e) {
            System.out.println("Insert Waypoint:" + e);
        }
    }

    // insert a waypoint from the UI based on the completed fields
    @FXML
    public void insertWaypoint(MouseEvent event) {
        if (waypointNameBox.getText().equals(""))
            return;
        try {
            double x = Double.parseDouble(xFieldWaypoint.getText());
            double y = Double.parseDouble(yFieldWaypoint.getText());

            // width and height of the render pane
            if (x > 950 || y > 750) {
                return;
            }

            Waypoint waypoint = new Waypoint(
                    x,
                    y,
                    waypointNameBox.getText());

            waypointService.insertWaypoint(waypoint);
            waypointListView.getItems().add(waypoint.getName());
            generalWaypointListview.getItems().add(waypoint.getName());
            xFieldWaypoint.setText("0");
            yFieldWaypoint.setText("0");
            waypointNameBox.setText("");
            renderWaypoint(waypoint);
        } catch (NumberFormatException e) {
            System.out.println("Insert Waypoint:" + e);
        }
    }

    // Renders the waypoint on the renderPane
    public void renderWaypoint(Waypoint waypoint) {
        Circle waypointMarker = new Circle(
                waypoint.getX(),
                waypoint.getY(),
                55,
                javafx.scene.paint.Color.YELLOW
        );
        renderPane.getChildren().add(waypointMarker);
        // so that the name of the waypoints is aprox in the middle of the circle that represents the waypoint
        if (waypoint.getX() > 15)
            renderPane.getChildren().add(new Text(waypoint.getX() - 15, waypoint.getY(), waypoint.getName()));
        else
            renderPane.getChildren().add(new Text(waypoint.getX(), waypoint.getY(), waypoint.getName()));
    }

    // constructs the UI represantation of the vehicle and its properties
    public void startVehicle(Vehicle vehicle) {
        // the vehicle representation is a stackpane that contains the model and the name
        StackPane stackPane = new StackPane();
        Rectangle vehicleModel = new Rectangle(
                0,
                0,
                60,
                30
        );
        stackPane.setLayoutX(vehicle.getCurrentPos().getX());
        stackPane.setLayoutY(vehicle.getCurrentPos().getY());
        vehicleModel.setFill(vehicle.getColor());
        vehicleModel.setStyle("-fx-stroke: black; -fx-stroke-width: 5;");

        Text name = new Text(vehicle.getName());
        stackPane.getChildren().addAll(vehicleModel, name);

        // draw vehicle path
        Line line = new Line();
        line.setStrokeWidth(10);
        line.setStroke(vehicle.getColor());

        renderPane.getChildren().add(line);
        renderPane.getChildren().add(stackPane);
        // start a thread which will make the vehicle move
        renderedVehicles.add(new RenderedVehicle(vehicle.getName(), stackPane, line));
    }

    // updates the render pane based of the information it gets from the rendered vehicles list
    void updateRenderScreen() {
        lock.writeLock().lock();
        Vehicle vehicle;
        Line line;
        StackPane stackPane;
        for (RenderedVehicle renderedVehicle:renderedVehicles) {
            vehicle = vehicleService.getVehicle(renderedVehicle.getVehicleName());
            line = renderedVehicle.getLine();
            stackPane = renderedVehicle.getStackPane();

            // update road data
            line.setStartX(vehicle.getCurrentPos().getX());
            line.setStartY(vehicle.getCurrentPos().getY());
            if (vehicle.getDestination() != null) {
                line.setEndX(vehicle.getDestination().getX());
                line.setEndY(vehicle.getDestination().getY());
            } else {
                line.setEndX(vehicle.getCurrentPos().getX());
                line.setEndY(vehicle.getCurrentPos().getY());
            }

            // update pane location that represents the vehicle with the actual changed value from the thread
            stackPane.setLayoutX(vehicle.getCurrentPos().getX());
            stackPane.setLayoutY(vehicle.getCurrentPos().getY());
        }
        lock.writeLock().unlock();
    }

    // in UI speed change handler
    @FXML
    void generalChangeSpeed(MouseEvent event) {
        lock.writeLock().lock();
        String name = generalVehicleComboBox.getSelectionModel().getSelectedItem();
        Vehicle vehicle = vehicleService.getVehicle(name);
        if (vehicle == null)
            return;
        speedInFocus = false;
        vehicle.setSpeed(Double.parseDouble(generalVehicleSpeed.getText()));
        lock.writeLock().unlock();
    }

    // in UI direction change handler
    @FXML
    void generalChangeDestination(MouseEvent event) {
        lock.writeLock().lock();
        lock.readLock().lock();

        String name = generalVehicleComboBox.getSelectionModel().getSelectedItem();
        if (name == null) {
            lock.writeLock().unlock();
            lock.readLock().unlock();
            return;
        }
        Vehicle vehicle = null;
        for (RenderedVehicle renderedVehicle:renderedVehicles) {
            if (renderedVehicle.getVehicleName().equals(name))
                vehicle = vehicleService.getVehicle(renderedVehicle.getVehicleName());
        }
        if (vehicle == null)
            return;
        Waypoint newDest = waypointService.getWaypoint(generalWaypointListview.getSelectionModel().getSelectedItem());
        if (newDest == null) {
            lock.writeLock().unlock();
            lock.readLock().unlock();
            return;
        }

        vehicle.setDestination(newDest);
        lock.writeLock().unlock();
        lock.readLock().unlock();
    }

    // reloads the data about the selected vehicle into the general information tab
    @FXML
    void generalLoadVehicle(ActionEvent event) {
        lock.writeLock().lock();
        String name = generalVehicleComboBox.getSelectionModel().getSelectedItem();

        Vehicle vehicle = null;
        for (RenderedVehicle renderedVehicle:renderedVehicles) {
            if (renderedVehicle.getVehicleName().equals(name))
                vehicle = vehicleService.getVehicle(renderedVehicle.getVehicleName());
        }

        if (vehicle == null) {
            lock.writeLock().unlock();
            return;
        }
        if (!speedInFocus)
            generalVehicleSpeed.setText(String.valueOf(vehicle.getSpeed()));
        if (vehicle.getCurrentPos() == null) {
            generalCurrentPosition.setText("");
        } else {
            generalCurrentPosition.setText(Math.round(vehicle.getCurrentPos().getX()) +
                    " " + Math.round(vehicle.getCurrentPos().getY()) +
                    " " + vehicle.getCurrentPos().getName()
            );
        }


        if (vehicle.getDestination() == null) {
            generalDestination.setText("");
        } else {
            generalDestination.setText(Math.round(vehicle.getDestination().getX()) +
                    " " + Math.round(vehicle.getDestination().getY()) +
                    " " + vehicle.getDestination().getName()
            );
        }
        lock.writeLock().unlock();
    }

    // helps with modifying the speed while it is getting constantly refreshed by other functions
    @FXML
    void setGeneralSpeedFocus(MouseEvent event) {
        speedInFocus = true;
    }
}
