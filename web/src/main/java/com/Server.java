package com;

import com.render.JavaFxApp;
import javafx.application.Application;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@AutoConfigureBefore
@SpringBootApplication
public class Server {
    public static void main(String[] args) {
        Application.launch(JavaFxApp.class, args);
    }
}
